import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AddSalesperson from './sales/AddSalesperson';
import ListSalesPeople from './sales/ListSalesPeople';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="salespeople/create" element={<AddSalesperson />} />
          <Route path="salespeople" element={<ListSalesPeople/>} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
