import React from "react"
import { useState, useEffect } from "react"

function AddSalesperson() {

    const [salespeople, setSalespeople] = useState([])

    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        employee_id: '',
    })

    async function getSalespeople(){
        const url = "http://localhost:8090/api/salespeople/"
        const response = await fetch(url)
        // console.log(response)

        if(response.ok) {
            const salesPersonData = await response.json()
            // console.log(salesPersonData)
            setSalespeople(salesPersonData.salespeople)
        }
    }

    useEffect(() => {
        getSalespeople()
    }, [])


    const handleSubmit = async (e) => {
        e.preventDefault()

        const url = "http://localhost:8090/api/salespeople/"

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch (url, fetchConfig)
        const salesPerson = response.json()
        console.log("this is a salesperson", salesPerson)
        // ^^^ returns a promise that has been fulfilled w the new salesperson data 
        if(response.ok) {
            setFormData({
                first_name: '',
                last_name: '',
                employee_id: '',
            })
        }
    }

    const handleFormChange = async (e) => {
        const input = e.target.name
        const value = e.target.value
        // console.log("This is input and value: ", input, value)

        setFormData({
            ...formData,
            [input]: value,
        })

    }


    return(
        <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create Salesperson</h1>
                        <form onSubmit={handleSubmit} id="create-salesperson-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.first_name} placeholder="Salesperson first name" required type="text" name="first_name" id="first_name" className="form-control" />
                                <label htmlFor="first_name">Salesperson first name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.last_name} placeholder="Salesperson last name" required type="text" name="last_name" id="last_name" className="form-control" />
                                <label htmlFor="first_name">Salesperson last name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.employee_id} placeholder="Employee ID" required type="text" name="employee_id" id="employee_id" className="form-control" />
                                <label htmlFor="employee_id">Employee ID</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )

}
export default AddSalesperson
