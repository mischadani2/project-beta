import React from "react";
import { useState, useEffect } from "react";


function ListSalesPeople() {

    const [salespeople, setSalespeople] = useState([])

    const getSalespeopleData = async () => {
        const url = "http://localhost:8090/api/salespeople/"
        const response = await fetch(url)

        if(response.ok) {
            const salespeopleData = await response.json()
            setSalespeople(salespeopleData.salespeople)
        }
    }

    useEffect(() => {
        getSalespeopleData()
    }, [])

    return(
        <>
        <h1>Salespeople</h1>
        <table className="table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Employee ID</th>
                </tr>
            </thead>
            <tbody>
                {salespeople.map(salesperson => {
                    return(
                        <tr key={salesperson.id}>
                            <td>{salesperson.first_name} {salesperson.last_name} </td>
                            <td>{salesperson.employee_id}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        </>
    )
}

export default ListSalesPeople
