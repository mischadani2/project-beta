from django.urls import path
from .views import (
    list_and_create_customer,
    list_and_create_salesperson,
    list_and_create_sales,
    delete_customer,
    delete_salesperson,
    delete_customer,
    delete_sale,
    show_salesperson,
    show_customer,
    show_sale
)

urlpatterns = [
    path("customers/", list_and_create_customer, name="list_and_create_customer"),
    path("customers/<int:id>/", show_customer, name="show_customer"),
    path("customers/<int:id>/", delete_customer, name="delete_customer"),
    path("salespeople/", list_and_create_salesperson, name="list_and_create_salesperson"),
    path("salespeople/<int:id>/", show_salesperson, name="show_salesperson"),
    path("salespeople/<int:id>/", delete_salesperson, name="delete_salesperson"),
    path("sales/", list_and_create_sales, name="list_and_create_sales"),
    path("sales/<int:id>/", show_sale, name="show_sale"),
    path("sales/<int:id>/", delete_sale, name="delete_sale"),
]
