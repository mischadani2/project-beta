from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import AutomobileVO, Salesperson, Customer, Sale
from common.json import ModelEncoder
# Create your views here.

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id",
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "automobile",
        "salesperson",
        "customer",
        "id",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
    }

    def get_extra_data(self, o):
        return {
            "vin": o.automobile.vin,
        }


#SALESPERSON
# List, Create, See a salesperson, Delete
@require_http_methods(["GET", "POST"])
def list_and_create_salesperson(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
        )
    else:
        new_salesperson = json.loads(request.body)
        salesperson = Salesperson.objects.create(**new_salesperson)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False
        )

@require_http_methods("GET")
def show_salesperson(request, id):
    salesperson = Salesperson.objects.filter(id=id)
    return JsonResponse(
         salesperson,
         encoder=SalespersonEncoder,
         safe=False,
    )

@require_http_methods("DELETE")
def delete_salesperson(request, id):
        count, _ = Salesperson.objects.filter(id=id).delete()
        return JsonResponse(
             {"deleted": count > 0}
        )



#CUSTOMER
# List, create, delete
@require_http_methods(["GET", "POST"])
def list_and_create_customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        new_customer = json.loads(request.body)
        customer = Customer.objects.create(**new_customer)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False
        )

@require_http_methods("GET")
def show_customer(request, id):
    customer = Customer.objects.filter(id=id)
    return JsonResponse(
         customer,
         encoder=CustomerEncoder,
    )


@require_http_methods("DELETE")
def delete_customer(request, id):
        count, _ = Customer.objects.filter(id=id).delete()
        return JsonResponse(
             {"deleted": count > 0}
        )


#SALES
# List, create, delete
@require_http_methods(["GET", "POST"])
def list_and_create_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            sales_id = content["salesperson"]
            id = Salesperson.objects.get(id=sales_id)
            content["salesperson"] = id
            print(id)

        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson does not exist"}
            )
        try:
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer

        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"}
            )
        try:
            print("*************We are in the try block")
            vin_num = content["automobile"]
            # print("vin num", vin_num)
            vin = AutomobileVO.objects.get(vin=vin_num)
            print("vin", vin)
            content["automobile"] = vin
            print("this is content", content)
            print(AutomobileVO)

        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "AutomobileVO does not exist"},
                    status=400
            )


        sale = Sale.objects.create(**content)
        print(sale)

        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )



    #     try:
    #         automobile = AutomobileVO.objects.get(vin=sale["vin"])
    #         print("************************", automobile)

    #         salesperson = Salesperson.objects.get(employee_id=sale["salesperson"])


    #         customer = Customer.objects.get(id=sale["customer"])


    #     except AutomobileVO.DoesNotExist or Customer.DoesNotExist or Salesperson.DoesNotExist:
    #         response = JsonResponse(
    #             {"message": "Either Automobile, Salesperson or Customer does not exist"}
    #         )


    # new_sale = Sale.objects.create(**sale)

    # return JsonResponse(
    #     new_sale,
    #     encoder=SaleEncoder,
    #     safe=False
    # )

@require_http_methods("GET")
def show_sale(request, id):
    sale = Sale.objects.filter(id=id)
    return JsonResponse(
         sale,
         encoder=SaleEncoder,
         safe=False,
    )

@require_http_methods("DELETE")
def delete_sale(request, id):
        count, _ = Sale.objects.filter(id=id).delete()
        return JsonResponse(
             {"deleted": count > 0}
        )
